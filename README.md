# README #

Install the dependencies and download the folder to check the state-of-the art deep learning architecture. Read from the reference files and see ideas are implemented through code.

### What is this repository for? ###

*Understanding of deep learning application in computer vision
*Reproducing results obtained
*Formulation of what ideas and concepts can be useful to try in future

### How do I get set up? ###

* Summary of set up

It requires OpenCV2.0 install for implementing some code . Rest all works fine with basic deep learning libraries (keras, tensorflow, theano)

* Dependencies

backports.weakref==1.0rc1
bleach==1.5.0
cycler==0.10.0
Cython==0.25.2
darkflow==1.0.0
h5py==2.7.0
html5lib==0.9999999
Keras==2.0.5
Markdown==2.2.0
matplotlib==2.0.2
numpy==1.13.0
olefile==0.44
pandas==0.20.2
Pillow==4.1.1
protobuf==3.3.0
pyparsing==2.2.0
python-dateutil==2.6.0
pytz==2017.2
PyYAML==3.12
scipy==0.19.0
six==1.10.0
tensorflow==1.2.0
Theano==0.9.0
Werkzeug==0.12.2

* How to run tests


python <file_name> --image <file_path>